<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $fotos=[
                'bici','bobina','caballito','cascos','comida','edificio','lunes'
            ];
            
            $numeroFotos = count($fotos);
            const TOTAL = 10;
            
            for ($contador = 0; $contador < TOTAL; $contador++){
                $indice = mt_rand(0, $numeroFotos - 1);
                $foto = $fotos[$indice];
        ?>
        <img src="imgs/<?= $foto ?>.jpg">
        <?php
            }
        ?>
        
    </body>
</html>
