<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // opcion 1
        if ($_GET["cal"]>=8) {
            echo "Aprobado";
        }else{
            echo "Suspenso";
        } 
        
        //opcion 2
        
        $salida="";
        if ($_GET["cal"]>=8) {
            $salida="aprobado";
        } else {
        $salida="suspenso";    
        }
        
        //opcion 3
        
        $salida="suspenso";
        if ($_GET["cal"]>=8) {
            $salida="aprobado";
        }
        echo $salida;
        
        //opcion 4
        
        $salida="Aprobado";
        if ($_GET["cal"]>=8) {
            $salida="Suspenso";
        }
        echo $salida;
           
        
        ?>
    </body>
</html>
