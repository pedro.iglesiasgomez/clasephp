<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // creo una funcion sin argumentos 
        
        /**
         * que hace ==> Mostrar mensaje en pantalla
         * que recibe ==> nada
         * que devuelve ==> nada 
         */
        function mostrar(){
            echo "<div>Hola clase</div>";
        }
        
        // llamo a la funcion 3 veces
        mostrar();
        mostrar();
        mostrar();
        ?>
    </body>
</html>
