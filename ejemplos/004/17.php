<?php
    /**
     * Funcion que suma todos los elementos del array
     * @param array $numeros array con los numeros a sumar
     * @return int la suma de todos los numeros del array
     */
    function suma($numeros){
        $resultado=0;
        foreach($numeros as $valor){
            $resultado+=$valor;
        }
        return $resultado;
    }
    
    function producto($numeros){
        $resultado=1;
        foreach($numeros as $valor){
            $resultado*=$valor;
        }
        return $resultado;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $edades=[28,29,32,19,24];
        $sumados=suma($edades);
        $multiplicados= producto($edades);
        ?>
        <ul>
            <li>Suma: <?= $sumados ?></li>
            <li>Producto: <?= $multiplicados ?></li>
        </ul>
        <ul>
            <li>Suma: <?= suma([23,2,5]) ?></li>
            <li>Producto: <?= producto([2,3]) ?></li>
        </ul>
    </body>
</html>
