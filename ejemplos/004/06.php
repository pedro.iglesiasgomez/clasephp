<?php
/**
 * Muestra dos numeros que le paso como argumentos
 * @param float $numero1 el primer numero a mostrar
 * @param float $numero2 el segundo numero a mostrar por defecto es 10
 */
function mostrar($numero1,$numero2=10){
    echo "numero1: {$numero1}<br>";
    echo "numero2: {$numero2}<br>";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        mostrar(12); // numero1=12  numero2=10==> el 10 es el numero por defecto 
        mostrar(13,5); // numero1=13  numero2=5
        ?>
    </body>
</html>
