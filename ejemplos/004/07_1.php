<?php
/**
 * Muestra dos numeros incrementandoles una unidad
 * @param int &$numero1 argumento pasado por referencia
 * @param int $numero2 argumento pasado por valor
 */
function mostrar(&$numero1,$numero2=10){ //&$numero1==> argumento pasado por referencia
    $numero1++; // al sumar 1 a numero1 estoy sumando 1 a n1
    $numero2++;
    echo "numero1: {$numero1}<br>"; 
    echo "numero2: {$numero2}<br>";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $n1=10;
        $n2=50;
        mostrar($n1); // $numreo1=&$n1==>10  numero2=10 
        mostrar($n1,$n2); // numero1=&$n1==>11  numero2=50
        echo "n1: {$n1}<br>";
        echo "n2: {$n2}<br>";
        ?>
    </body>
</html>
