<!DOCTYPE html>
<?php
    /**
     * Funcion que calcula la media de los VALORES DE UN ARRAY
     * @param array $numeros array con los numeros
     * @return float el valor medio calculado
     */
    function promedio($numeros){
        $media=0; // almacenar la media
        $suma=0; // almacenar la suma
        $cantidad=count($numeros); // elementos del array
        foreach($numeros as $valor){
            $suma=$suma+$valor;
            //$suma+=$valor;
        }
        $media=$suma/$cantidad;
        return $media;
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $edades=[23,24,25,56];
        $media=promedio($edades);
        echo $media;
        ?>
    </body>
</html>

   
