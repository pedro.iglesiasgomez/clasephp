<?php
    /**
     * Funcion que suma todos los elementos del array
     * @param array $numeros array con los numeros a sumar
     * @return int la suma de todos los numeros del array
     */
    function suma($numeros){
        $resultado=0;
        foreach($numeros as $valor){
            $resultado+=$valor;
        }
        return $resultado;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $edades=[28,29,32,19,24];
        echo suma($edades);
        ?>
    </body>
</html>
