<?php
    function repetir($veces=1,$caracter="*"){
        $salida="";
        for($c=0;$c<$veces;$c++){
            $salida="{$salida} {$caracter}";
        }
        return $salida;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $valores=[1,10,23,20,18,34];
        $maximo= max($valores);
        echo "<div>";
        for($c=0;$c<=$maximo;$c++){
            echo "{$c} ";
        }
        echo "</div>";
        foreach ($valores as $valor){
        ?>
        <div><?= repetir($valor) ?></div>
        <?php
        }
        ?>

    </body>
</html>
