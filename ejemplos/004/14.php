<?php
    function repetir($veces=1,$caracter="*"){
        $salida="";
        for($c=0;$c<$veces;$c++){
            $salida="{$salida} {$caracter}";
        }
        return $salida;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div><?= repetir() ?></div>
        <div><?= repetir(10) ?></div>
        <ol>
            <li><?= repetir(12,"-") ?></li>
            <li><?= repetir(12) ?></li>
            <li><?= repetir(10,"#") ?></li>
        </ol>
    </body>
</html>
