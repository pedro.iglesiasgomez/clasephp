<?php
    /*
     * Ejemplo de etiqueta a crear
     * <div style="background-color: coral;
     *              width:400px;text-align: center;margin:10px auto">
     * </div>
     */

    function salidaCentrada($texto,$ancho=400,$fondo="coral"){
        $estilos="width:{$ancho}px"; // colocamos el ancho
        $estilos=$estilos . ";background-color:{$fondo}"; // colocamos el color azul
        $estilos=$estilos . ";margin:10px auto"; // colocamos la caja centrada
        $estilos=$estilos . ";text-align:center"; // colocamos el texto centrado
        $etiqueta='<div style="' . $estilos .  '">' . "{$texto}</div>";
        
        return $etiqueta;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo salidaCentrada("Texto",200);
        echo salidaCentrada("Ejemplo de clase",200);
        echo salidaCentrada("Ejemplo de 300",400,"blue");
        ?>
    </body>
</html>
