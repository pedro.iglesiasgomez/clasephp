<?php
    require_once './ejemplo7/Animal.php';
    require_once './ejemplo7/Ciudad.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            //utilizar la clase
            //creamos un objeto de tipo animal
           $animal= new Animal();
                   
           $animal->setRaza("Perro");
           $animal->setColor("Blanco Con machas marrones");
           $animal->setNombre("Zeus");
           $animal->setFechaNacimiento("01/01/2021");
           
           // mostrar la imformacio introducida
           echo $animal->getNombre();
           
           echo $animal->datos();
           
           // crear otro objeto
           $animal1= new Animal();
           
           $animal1->setRaza("Perro");
           $animal1->setColor("negro");
           $animal1->setNombre("Ares");
           $animal1->setFechaNacimiento("01/01/2020");
           
           // mostrar la imformacio introducida
           echo $animal1->getNombre();
           echo $animal1->datos();
           
           // Crear objeto tipo Ciudad
           $poblacion= new Ciudad();
           
           $poblacion->setNombre("Santander");
           $poblacion->setProvincia("Cantabria");
           
           echo $poblacion->getNombre();
           echo $poblacion->obtenerIniciales();
          // echo $poblacion->obtenerIniciales(5);
           
           $poblacion1= new Ciudad();
           
           $poblacion1->setNombre("Oviedo");
           $poblacion1->setProvincia("Asturias");
           
           echo $poblacion1->getNombre();
           echo $poblacion1->obtenerIniciales();
           //echo $poblacion1->obtenerIniciales(5);
        ?>      
    </body>
</html>

