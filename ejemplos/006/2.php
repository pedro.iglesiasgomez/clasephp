<?php
// Creando una clase
    class Persona{
        //propiedades publicas
        public $nombre=null;
        public $apellido=null;
        public $edad;
        
        //propiedad privada
        private $tratamiento="Sr/a";
        
        // metodo constructor
        public function __construct($nombre=" ", $apellido=" ", $edad=0) {
            $this->nombre = $nombre;
            $this->apellido = $apellido;
            $this->edad = $edad;
        }

        
        // metodo publico
        
        // getter
        public function getNombre(){
            return $this->tratamiento . " " . $this->nombre;
        }
        
        // setter
        public function setNombre($nombre){
            $this->nombre = strtoupper($nombre);
        }
        
        public function nombreCompleto() {
            return $this->getNombre() . " " . $this->apellido;
        }
        
        public function datos() {
            echo "<ul>";
            echo "<li>Nombre :" . $this->nombre . "</li>";
            echo "<li>Apellidos :" . $this->apellido . "</li>";
            echo "<li>Edad :" . $this->edad . "</li>";
            echo "<li> Iniciales : " . $this->calcularIniciales() . "</li>";
            echo "</ul>";
        }

        //metodo privado
        private function calcularIniciales() {
            return $this->nombre[0] . ". " . $this->apellido[0] . ". ";
            //return $this-> subsring(nombre, 0,1) . " " . $this->substring(apellido,0,1) . ". ";
        }
      
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
           
        // crear un objeto
        // instanciar
        
        $persona1=new Persona();
        //var_dump($persona1);
        
        
        $persona2=new Persona("Susana");
        //var_dump($persona2);
        
        $persona3=new Persona("Pablo", "Lopez");
        //var_dump($persona3);
        
        $persona4=new Persona("Eva","Vazquez",35);
        //var_dump($persona4);
        
        $persona1->datos();
        $persona2->datos();
        $persona3->datos();
        $persona4->datos();
        
        
        ?>
    </body>
</html>
