<?php
    spl_autoload_register(function ($nombre_clase) {
        include $nombre_clase . '.php';
});

use ejemplo9\Circulo;

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php             
            $unCirculo= new Circulo(3.4, 2, 2);
            echo "<br>Area: " . $unCirculo->Area();
            echo "<br>Perimetro: " . $unCirculo->Perimetro();
            echo $unCirculo->getRadio();
            
            $unCirculo->pintar();
            
            
            $unCirculo1= new Circulo(15, 30, 50);
            $unCirculo1->pintar();        
        ?>
    </body>
</html>
