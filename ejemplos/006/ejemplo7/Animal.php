<?php
/**
 * Description of animales
 *
 *
 */
class Animal {
        
        //propiedades
        public $nombre;    
        private $raza;
        protected $color;
        public $fechaNacimiento;
        
        //Metodos Getters
        
        public function getNombre() {
            return $this->nombre;
        }

        public function getRaza() {
            return $this->raza;
        }

        public function getColor() {
            return $this->color;
        }

        public function getFechaNacimiento() {
            return $this->fechaNacimiento;
        }
        
        //Metodos Setters
        
        public function setNombre($nombre) {
            $this->nombre = $nombre;
        }

        public function setRaza($raza) {
            $this->raza = $raza;
        }

        public function setColor($color) {
            $this->color = $color;
        }

        public function setFechaNacimiento($fechaNacimiento) {
            $this->fechaNacimiento = $fechaNacimiento;
        }
        
        public function datos() {
            $resultado="<ul>";
            $resultado=$resultado . "<li>La raza del animal es " . $this->raza . "</li>";
            $resultado=$resultado . "<li>El color es {$this->color} </li>";
            $resultado.="<li>Su nombre es " . $this->nombre . "</li>";
            $resultado.="</ul>";
            
            return $resultado;
        }

    }

