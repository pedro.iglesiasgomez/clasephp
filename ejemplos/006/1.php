<?php
// Creando una clase
    class Persona{
        //propiedades publicas
        public $nombre=null;
        public $apellido=null;
        public $edad;
        
        //propiedad privada
        private $tratamiento="Sr/a";
        
        
        // metodo publico
        
        // getter
        public function getNombre(){
            return $this->tratamiento . " " . $this->nombre;
        }
        
        // setter
        public function setNombre($nombre){
            $this->nombre = strtoupper($nombre);
        }
        
        public function nombreCompleto() {
            return $this->getNombre() . " " . $this->apellido;
        }
        
        public function datos() {
            echo "<br>Nombre :" . $this->nombre;
            echo "<br>Apellidos :" . $this->apellido;
            echo "<br>Edad :" . $this->edad;
            echo "<br> Iniciales: " . $this->calcularIniciales();
        }

        //metodo privado
        private function calcularIniciales() {
            return $this->nombre[0] . ". " . $this->apellido[0] . ". ";
            //return $this-> subsring(nombre, 0,1) . " " . $this->substring(apellido,0,1) . ". ";
        }
      
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
           
        // crear un objeto
        // instanciar
        
        $persona1=new Persona();
        var_dump($persona1);
        
        // introducir datos en persona1
        $persona1->nombre="Susana";
        $persona1->apellido="Lopez";
        
        // mostrando los datos
        $persona1->datos();
        
        // mostrando el nombre
        echo "<br>" . $persona1->getNombre();
        
        // introduciendo el nombre a traves de setter
        $persona1->setNombre("Susana");
        
        // mostrando el nombre
        echo "<br>" . $persona1->getNombre();
        
        ?>
    </body>
</html>
