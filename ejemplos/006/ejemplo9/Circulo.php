<?php

/**
 * Description of Circulo
 *
 */

namespace ejemplo9;

class Circulo {
    private $radio;
    private $centoX;
    private $centroY;
    
    public function __construct($radio=50, $centroX=25, $centroY=25) {
        $this->radio = $radio;
        $this->centoX = $centroX;
        $this->centroY = $centroY;
    }
    
    public function getRadio() {
        return $this->radio;
    }

    public function getCentoX() {
        return $this->centoX;
    }

    public function getCentroY() {
        return $this->centroY;
    }

    public function setRadio($radio) {
        $this->radio = $radio;
    }

    public function setCentoX($centoX) {
        $this->centoX = $centoX;
    }

    public function setCentroY($centroY) {
        $this->centroY = $centroY;
    }
    
    public function Area() {
        $resultado=0;
        $resultado=M_PI*$this->radio**2;
        return $resultado;
    }
    
    public function Perimetro() {
        $resultado=0;
        $resultado=2*M_PI*$this->radio;
        return $resultado;
    }
    
    public function pintar() {
       echo '<svg height="400" width="400">';
       echo '<circle  cx="'. $this->centroX .'" cy="'. $this->centroY .'" r="'. $this->radio .'" stroke="black" stroke-width="3" fill="red" />';
       echo '</svg>';
    }
}
