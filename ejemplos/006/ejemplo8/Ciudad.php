<?php
/**
 * Description of Ciudad
 *
 *
 */

namespace ejemplo8;


class Ciudad {
    public $nombre;
    private $provincia;
    
    //Metodos
    //getters
    
    public function getNombre() {
        return $this->nombre;
    }

    public function getProvincia() {
        return $this->provincia;
    }
    
    //setters
    
    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setProvincia($provincia) {
        $this->provincia = $provincia;
    }
    
    public function obtenerIniciales() {
        $resultado="<ul>";
        $resultado.="<li>Las Iniciales de la ciudad son: " . substr($this->nombre, 0, 2) . "</li>";
        $resultado.="<li>Las Iniciales de la provincia son: " . substr($this->provincia, 0, 2) . "</li>";       
        $resultado.="</ul>";
        
        return $resultado;
    }
   /* public function obtenerIniciales($numero) {
       $resultado=substr($this->nombre, 0, $numero) . " " . substr($this->provincia,0,$numero); 
    
        return $resultado;
    }*/
    
}
