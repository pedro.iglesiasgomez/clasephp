<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejemplo9_1" method="get">
            <div>
            <label for="numero1">introduce numero 1</label>
            <input type="number" id="numero1" name="vnumero1" min="0" max="100" step="1" required>
            </div>
            <div>
            <label for="numero2">introduce numero 2</label>
            <input type="number" id="numero2" name="vnumero2" min="0" max="100" step="1" required>
            </div>
            <div>
            <label for="numero3">introduce numero 3</label>
            <input type="number" id="numero3" name="vnumero3" min="0" max="100" step="1" required>
            </div>
            <div>
            <label for="numero4">introduce numero 4</label>
            <input type="number" id="numero4" name="vnumero4" min="0" max="100" step="1" required>
            </div>
            <div>
            <label for="numero5">introduce numero 5</label>
            <input type="number" id="numero5" name="vnumero5" min="0" max="100" step="1" required>
            </div>
            <div>
                <button>Enviar</button>
                <button type="reset">Limpiar</button>
            </div>
        </form>    
    </body>
</html>
