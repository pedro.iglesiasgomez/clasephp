<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejemplo10_1.php" method="get">
            <div>
            <label for="numero1">introduce numero 1</label>
            <input type="number" id="numero1" name="numero[]" min="0" max="100" step="1" required>
            </div>
            <div>
            <label for="numero2">introduce numero 2</label>
            <input type="number" id="numero2" name="numero[]" min="0" max="100" step="1" required>
            </div>
            <div>
            <label for="numero3">introduce numero 3</label>
            <input type="number" id="numero3" name="numero[]" min="0" max="100" step="1" required>
            </div>
            <div>
            <label for="numero4">introduce numero 4</label>
            <input type="number" id="numero4" name="numero[]" min="0" max="100" step="1" required>
            </div>
            <div>
            <label for="numero5">introduce numero 5</label>
            <input type="number" id="numero5" name="numero[]" min="0" max="100" step="1" required>
            </div>
            <div>
                <button>Enviar</button>
                <button type="reset">Limpiar</button>
            </div>
        </form>    
    </body>
</html>
