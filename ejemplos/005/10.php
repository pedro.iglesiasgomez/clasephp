<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
            Crear una funcion que me indique el numero de veces que se repite un numero de un array
            Sintaxis de la funcion:
            int contar (array con numeros, numero a buscar);
            Para realizarlo creo un array con numeros:
            $x=[1,2,3,4,1,2,3,4,1,1];
            Buscar las veces que se repite el 1.
        </pre>
        <?php
        $x=[1,2,3,4,1,2,3,4,1,1];
        $repe=0;
        function numeroArray ($numeros, $numero){
            $cantidad=0;
            foreach ($numeros as $repeticion){
                if ($repeticion==$numero){;
                    $cantidad++;
                }    
            } 
            return $cantidad;
        }
        echo "<br>numero de veces que se repite un numero en un array<br>";
        $repe= numeroArray($x, 1);
        var_dump($repe);        
        
        function numeroArray1 ($numeros, $numero){
            $resultado= array_count_values($numeros);
            return $resultado[$numero];
        }
        echo "<br>numero de veces que se repite un numero en un array con array_count_values<br>";
        var_dump(numeroArray1($x, 1))
        
        ?>
    </body>
</html>
