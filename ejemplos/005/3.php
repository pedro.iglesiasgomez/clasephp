<?php
    function operacion ($numero1,$numero2,$operacion){
        $resultado=0;
        switch ($operacion){
            case 'suma':
                $resultado=$numero1+$numero2;
                break;
            case 'producto':
                $resultado=$numero1*$numero2;
                break;
            default :
                $resultado="operacion no implementada";
        }    
        return $resultado;          
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo operacion(2, 5, "suma") . "<br>";
        // echo "<br>";
        echo operacion(5, 4, "producto"). "<br>";
        // echo "<br>";    
        echo operacion(33, 4, "resta"). "<br>";
        // echo "<br>";
        ?>
    </body>
</html>
