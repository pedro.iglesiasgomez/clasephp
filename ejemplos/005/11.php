<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
        realizar una funcion que calcule la modas de un array pasado como argumento
        el array es
        $x=[1,2,3,4,1,2,3,4,1,1];
        La sintaxis de la funcion:
        int moda(array a utilizar)
        </pre>
        <?php
            function moda ($vector){
                $a=array_count_values($vector);
                $repeticion=max($a);
                $resultado=array_search($repeticion, $a);
                return $resultado;
            }
            
            function moda1($vector){
                $repeticiones=array_count_values($vector);
                $modaCalculada=0;
                $repeticionesMax=0;
                
                foreach($repeticiones as $incice=>$valor){
                    if($valor>$repeticionesMax){
                       $repeticionesMax=$valor;
                       $modaCalculada=$incice;
                    }
                }
                return $modaCalculada;
            }
            
            $x=[1,2,3,4,1,2,3,4,1,1];
            $salida=0;
            $salida=moda1($x);
            var_dump($salida);        
        ?>
    </body>
</html>
