<?php
    function operacion($a,$b,$c){
        $c=$a+$b; // $c=30   las suam que realizo no sirve para nada
    }
    
    function operacionConRetorno ($a,$b){
        $c=$a+$b;
        return $c; // devuelvo la suma
    }
    
    function operacionReferencia ($a,$b,&$c){
        $c=$a+$b; 
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $numero1=20;
            $numero2=10;
            $resultado=0;
            operacion($numero1, $numero2, $resultado); // $a=20  $b=10  $c=0
            echo $resultado; // vale 0
            
            $resultado=operacionConRetorno($numero1, $numero2);
            echo $resultado; // vale 30
                    
            $resultado=0;
            operacionReferencia($numero1, $numero2, $resultado);// los dos primeros argumentos los paso por valor, el tercero lo paso por referncia
            echo $resultado; // vale 30        
        ?>
    </body>
</html>
