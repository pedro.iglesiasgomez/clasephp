<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        $a=[
            "Lunes" => 100,
            "Martes" => 150,
            "Miercoles" => 300,
            "Jueves" => 10,
            "Viernes" => 50,
        ];
        
        $b="Miercoles";
        
        if(array_key_exists($b, $a)){
            echo $a[$b];
        }else{
            echo "No se";
        }
        
        /*
         * if(array_key_exists($b, $a)){
            echo $a[$b];
        }else{
            echo "No se";
         * Devuelve 300
         */
        
        /*if(array_key_exists($b, $a)){
            echo $a[$b];
        }else{
            echo "No se";
         * 
         * Salta el xdebug de que ha fallado y debajo "No se"      
         */     
       
        ?>
    </body>
</html>
