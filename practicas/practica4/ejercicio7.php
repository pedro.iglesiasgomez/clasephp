<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        $dia=8;
        
        // Opcion con if
        
        if($dia==1){
            $salida="Lunes";
        }elseif($dia==2){
            $salida="Martes";
        }elseif($dia==3){
            $salida="Miercoles";
        }elseif($dia==4){
            $salida="Jueves";    
        }elseif($dia==5){
            $salida="Viernes";
        }elseif($dia==6){
            $salida="Sabado";
        }elseif($dia==7){
            $salida="Domingo";    
        }else{
            $salida="No es ningun dia de la semana";
        }
         echo $salida;   
       
        
        
        // Opcion con Switch
         
        switch ($dia) {
            case 1:
                echo "Lunes";
                break;
            case 2:
                echo "Martes";
                break;
            case 3:
                echo "Miercoles";
                break;
            case 4:
                echo "Jueves";
                break;
            case 5:
                echo "Viernes";
                break;
            case 6:
                echo "Sabado";
                break;
            case 7:
                echo "Domingo";
                break;
            default:
                echo "No es un dia";
                break;
        }
        
        
        // Opcion con array
        
        $dias=[
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes",
            "Sabado",
            "Domingo",
            "No existe el dia"
        ];
        
        
        echo $dias[$dia-1];
        
        
        
        
        ?>
    </body>
</html>
