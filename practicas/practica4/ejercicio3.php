<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        $a=35;
        $b=["poco","algo","medio","mucho","enorme"];
        
        if($a<10){
            echo $b[0];
        }elseif($a<20){
            echo $b[1];
        }elseif($a<30){
            echo $b[2];
        }elseif($a<40){
            echo $b[3];
        }else{
            echo $b[4];
        }
        /* con $a=190 devuelve "enorme"
         * con $a=1 devuelve "poco"
         * con $a=25 devuleve "medio"
         * con $a=15 devuelve "algo"
         * con $a=35 devuelve "mucho"
         */ 
        
        ?>
    </body>
</html>
