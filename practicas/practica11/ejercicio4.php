<?php
    /**
     * Funcion que genera colores
     * @param int $numero El numero de colores a generar
     * @param bool $almohadilla=true con valor colores nos indica que coloquemos la almohadilla
     * @return array los colores solicitados en un array de cadenas
     */
   function generaColores($numero, $almohadilla=true){
       $colores=array();
       for($n=0;$n<$numero;$n++){
           $c=0; // contador para el segundo bucle
           $limite=6;
           $colores[$n]="";
           if($almohadilla){ // $almohadilla==true
               $colores[$n]="#";
               $limite=7;
           }
           for(;$c<$limite;$c++){
               $colores[$n].= dechex(mt_rand(0,15));
           }
       }
       return $colores;
   }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $salida= generaColores(5);
              
        var_dump($salida);
        ?>
    </body>
</html>
