<?php
    /**
     * Esta funcion genera una serie de numeros aleatorios
     * @param int $minimo Este es el valor minimo
     * @param int $maximo Este es el valor maximo
     * @param int $numero Numero de valores a generar
     * @return int[] conjunto de numero solicitados
     */
    function ejercicio1($minimo,$maximo,$numero){
        $local=array(); // array donde coloco el resultado
        
        // bucle para rellenar el array
        for ($c=0;$c<$numero;$c++){
            $local[$c]=mt_rand($minimo,$maximo);
        }
        // delvolver array
        return $local;
    }
    
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $salida=ejercicio1(1,10,10);
            var_dump($salida);
        ?>
    </body>
</html>
