<?php
    /**
     * Funcion que genera colores
     * @param int $numero El numero de colores a generar
     * @return array los colores solicitados en un array de cadenas
     */
   function generaColores($numero){
       $colores=array();
       for($n=0;$n<$numero;$n++){
           $colores[$n]="#";
           for($c=1;$c<7;$c++){
               $colores[$n].= dechex(mt_rand(0,15));
           }
       }
       return $colores;
   }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        var_dump(generaColores(10));
            
        ?>
    </body>
</html>
