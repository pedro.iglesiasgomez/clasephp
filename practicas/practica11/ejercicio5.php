<?php
    /**
     * Funcion que pasa un array y te vuelve los valores
     * @param $array Array de los elementos 
     * @param bool $devolverTodos 
     * @return int array con las frecuencias de los valores
     */
    function elementosRepetidos($array, $devolverTodos = false){
        $repeated = array();
        
        foreach ((array) $array as $value){
            $inArray = false;
            
            foreach ($repeated as $i => $rItem) {
                if ($rItem['value'] === $value) {
                    $inArray = true;
                    ++$repeated[$i]['count'];
                }
            }
            
            if (false === $inArray) {
                $i = count($repeated);
                $repeated[$i] = array();
                $repeated[$i]['value'] = $value;
                $repeated[$i]['count'] = 1;
            }
        }
        
        if (!$devolverTodos) {
            foreach ($repeated as $i => $rItem) {
                if ($rItem['count'] === 1) {
                    unset($repeated[$i]);
                }
            }
        }
        sort($repeated);
        
        return $repeated;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $entrada=[1,1,2,2,1,4];
        var_dump($repetir($entrada,true))
        ?>
    </body>
</html>
