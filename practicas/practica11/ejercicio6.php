<?php
/**
 * Fucion que retorna todos los elementos de un directorio como textos
 * @param string $handle Ruta relativa a la carpeta a estudiar
 * @return strin[] array con todos los elemenos de la carpeta a estudiar
 */
      function leerDirectorio($handle = "..") {
            $handle=opendir($handle);
            while (false !== ($archivo = readdir($handle))) {
                $archivos[] = strtolower($archivo);
            }
            closedir($handle);
            sort($archivos);
            return $archivos;
        }  
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        var_dump(leerDirectorio()); // muestra las carpetas del directorio ordenados
        var_dump(scandir("..")); // muestra las carpetas del directorio ordenados
        ?>
    </body>
</html>
