<?php
if (!empty($_REQUEST)){ // pregunta si has puslsado el boton
    if ($_REQUEST["numero"] > 0){
        $caso = "bien"; // se carga la primera vez
    } else {
        $caso = "mal"; // ya le has dado enviar
    }    
} else {
    $caso = "mal";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            input[type="number"]{
                width: 300px;
            }
            .obligatorio::before{
                content: "Obligatorio";
                min-width: 150px;
                display: inline-block;
            }
            .noObligatorio::before{
                content: "opcional";
                min-width: 150px;
                display: inline-block;
            }
        </style>
    </head>
    <body>
        <?php
        if ($caso == "bien") { // en el caso de que hayas pulsado el boton que te muestre la informacion aun que este vacio
            var_dump($_REQUEST);
        } else {
            ?>
            <div>
                <form name="f" >
                    <div class="obligatorio"><input required placeholder="Introduce un numero" step="1" min="1" max="100" type="number" name="numero" /></div> 
                    <div class="noObligatorio"><input placeholder="Introduce otro numero" step="1" min="1" max="100" type="number" name="numero1" /></div>
                    <input type="submit" value="Enviar" name="boton" />
                </form>
            </div>
            <?php
        }  /*
            * dos formularios uno en el que pone que es opcional y otro en el que pone opcional
            * esto es por el style que se hizo al principio en el que personalizas los div que hay despues.
            * dentro de las cajetillas te indica que debes introducir un numero que esta configurado en los div de antes  
            * esto es por el style que se hizo al principio       
            */
        ?>
    </body>
</html>
