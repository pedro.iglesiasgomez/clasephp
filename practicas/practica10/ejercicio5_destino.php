<?php
    define("RUTA", "http://127.0.0.1/poo2021/php/practicas/practica10");
    if(empty($_REQUEST)) { // con el empty determina si $_REQUEST esta vacia
        header("location: " . RUTA . "/ejercicio5.php"); // te redirecciona al formulario si no esta rellenado
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            // destinatario del formulario del apartado anterior
            extract($_REQUEST); // crea una variable de $_REQUEST
            //$nombre=$_REQUEST["nombre"];
                echo $nombre; // variable no inicializada
        ?>
    </body>
</html>
