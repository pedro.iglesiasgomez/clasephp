<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo __FILE__; // el directorio en el que se encuentra el ejercicio
        echo "<br>";
        echo __LINE__; // linea en el que esta escrita el echo
        echo "<br>";
        echo PHP_VERSION; // la version de php que se esta utilizando
        echo "<br>";
        echo PHP_OS;  // el sistema operativo para el que se hizo php
        echo "<br>";
        echo __DIR__; // directorio en el que esta la practica
        echo "<br>";
        ?>
    </body>
</html>
