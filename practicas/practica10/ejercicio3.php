<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            *{
                margin: 0px;
                padding: 0px;
            }
            
            span{
                width: 10px;
                height: 50px;
                background-color: #001ee7;
                display: inline-block;
                
            }
            div{
                height: 50px;
                display: inline-block;
                border: 5px solid #CCC;
                margin: 20px;
            }
        </style>
    </head>
    <body>
        <?php
        $datos = array(20, 40, 790, 234, 890);
        $todo = max($datos); // coje el mayor de los numeros del array
        foreach ($datos as $key => $value) { // recorre el array
            $datos[$key] = (int) (100 * $datos[$key] / $todo); // multiplica 100 por cada uno de los datos y los divide por el mas grande en este caso 890
        }    
        var_dump($datos); // muestra los valores de las posiciones de los numeros hasta 100
        /*
         * 0 => int 2
         * 1 => int 4
         * 2 => int 88
         * 3 => int 26
         * 4 => int 100
         */
        
        foreach ($datos as $value) { // recorre el array
            for ($c = 0; $c < $value; $c++) { // crea un bucle
                echo "<span></span>"; // coloca asterisco en funcion del valor de los numeros del array ejem: 20 tiene 2 por que su posicion en el array es 2
            }
            echo "</div>";
            echo "<br>"; 
        }
        // parecido al anterios solo que cambia los asteriscos por el formato de las tablas que se han puesto arriba en el style 
        
        ?>
    </body>
</html>
