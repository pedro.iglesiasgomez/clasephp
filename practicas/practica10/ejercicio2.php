<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $datos = array(20, 40, 790, 234, 890);
        $todo = max($datos); // coje el mayor de los numeros del array
        foreach ($datos as $key => $value) { // recorre el array
            $datos[$key] = (int) (100 * $datos[$key] / $todo); // multiplica 100 por cada uno de los datos y los divide por el mas grande en este caso 890
        }    
        var_dump($datos); // muestra los valores de las posiciones de los numeros hasta 100
        /*
         * 0 => int 2
         * 1 => int 4
         * 2 => int 88
         * 3 => int 26
         * 4 => int 100
         */
        
        foreach ($datos as $value) { // recorre el array
            for ($c = 0; $c < $value; $c++) { // crea un bucle
                echo "*"; // coloca asterisco en funcion del valor de los numeros del array ejem: 20 tiene 2 por que su posicion en el array es 2
            }
            echo "<br>"; 
        }
        ?>
    </body>
</html>
