<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $entero=10;
            $cadena="hola";
            $real=23.6;
            $logico=TRUE;
            
            var_dump($entero);
            var_dump($cadena);
            var_dump($real);
            var_dump($logico);
           /*
            * devuelve la informacion de los valores de las variables
            * $entero=10 ==> int 10
            * $cadena="hola" ==> string 'hola' (length=4)
            * $real=23.6 ==> float 23.6
            * $logico=TRUE ==> boolean true
            */              
            
            $logico=(int)$logico;
            $entero=(float)$entero;
            settype($logico, "int");
            
            var_dump($entero);
            var_dump($cadena);
            var_dump($real);
            var_dump($logico);
            
            /*
             * devuelve la informacion con las modificaciones que has puesto
             * $entero ==> float 10
             * $cadena="hola" ==> string 'hola' (length=4)
             * $real=23.6 ==> float 23.6
             * $logico=TRUE ==> int 1
             */
            
        ?>
    </body>
</html>
