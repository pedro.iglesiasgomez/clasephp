<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
           $alumno1= "Ramon";
           $alumno2= "Jose";
           $alumno3= "Pepe";
           $alumno4= "Ana";
           
           echo $alumno1;
           echo $alumno2;
           echo "<br>";
           echo $alumno3;
           echo "<br>";
           echo $alumno4;
        ?>
        <div>
            <?php
                echo "$alumno1<br>$alumno2<br>$alumno3<br>$alumno4";
            /*
             * devuleve dos veces los nombres con la diferencia de que la segunda vez estan
             * ordenados y la primera Ramon y Jose estan juntos
             * 
             * RamonJose
             * Pepe
             * Ana
             * Ramon
             * Jose
             * Pepe
             * Ana
             * 
             */     
            ?>            
        </div>
    </body>
</html>
