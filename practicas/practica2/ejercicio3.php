<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $radio=2.4;
        $perimetro= 2*M_PI*$radio;
        $area= pi()*$radio**2;        
        ?>
        <div>
            El radio del circulo es <?= $radio ?>
        </div>
        <div>
            El area del circulo es <?= $area ?>
        </div>
        <div>
            El perimetro del circulo es <?= $perimetro ?>
        </div>
    </body>
</html>
