<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2</title>
    </head>
    <body>
        <hl>Ejercicio 2 de la practica 1</hl>
        <table width="100%" border="1">
            <tr>
                <td>
                <?php
                // Podemos utilizar o comilla simples o comollas dobles para el texto
                echo "Este texto esta escrito utilizando la funcion echo en PHP";
                ?>
                </td>
                <td>Este texto esta escrito en HTML</td>
            </tr>
            <tr>
                <td>
                    <?php
                        print 'Este texto esta ecrito desde PHP con la funcion print';
                    ?>    
                </td>
                <td>
                    <?= "Centro de Formacion Alpe" ?>
                </td>
            </tr>
        </table>
        
    </body>
</html>
