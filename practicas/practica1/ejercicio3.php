<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercico 3</title>
    </head>
    <body>
        <h2>Metodo 1</h2>
        <?php
        // mezclamos html y php
        echo "<p> Hola mundo </p>";
        ?>
        <h2>Metodo 2</h2>
        <p>
            <?php
            // solo coloco php
            echo "Hola mundo";
            ?>
        </p>
        
        <h2>Metodo 3</h2>
        <?php
            echo "<p>";
        ?>
        
        <h2>Podemos utilzar el modo contraido del echo</h2>
        <p>
            <?= "Hola mundo" ?>
        </p>
        
        
    </body>
</html>
