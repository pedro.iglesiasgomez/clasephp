<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio1_9Destino.php" method="get">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" placeholder="Pon el nombre">
            </div>
            <br>
            <div>
                <label for="color">
                    Color:
                </label>
                <input type="text" name="color" placeholder="Pon un color">
            </div>
            <br>
            <div>
                <label for="alto">
                    Alto:
                </label>
                <input type="number" name="alto" placeholder="Pon la altura">
            </div>
            <br>
            <div>
                <label for="peso">
                    Peso:
                </label>
                <input type="number" name="peso" placeholder="Pon tu peso">
            </div>
            <br>
            <div>
                <input type="submit" value="Enviar">
            </div>   
    </body>
</html>
