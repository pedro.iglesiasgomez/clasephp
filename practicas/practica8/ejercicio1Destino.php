<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <!--
            Metodo largo
        -->
        Hola <?php echo $_GET["nombre"]; ?><br>
        Tu email es: <?php echo $_GET["email"]; ?>
        
        <br><br>
        <!--
            Metodo recotado
        -->
        Hola <?= $_GET["nombre"] ?><br>
        Tu email es: <?= $_GET["email"] ?>
    </body>
</html>
