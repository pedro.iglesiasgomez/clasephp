<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <!--
            Metodo largo
        -->
        Hola <?php echo $_GET["nombre"]; ?><br>
        Tu email es: <?php echo $_GET["email"]; ?><br>
        Telefono: <?php echo $_GET["telefono"]; ?>
        
        <br><br>
        <!--
            Metodo recotado
        -->
        Hola <?= $_GET["nombre"] ?><br>
        Tu email es: <?= $_GET["email"] ?><br>
        Telefono: <?= $_GET["telefono"] ?>
        
        <?php
            var_dump($_GET);
            var_dump($_GET);
        ?>
    </body>
</html>
