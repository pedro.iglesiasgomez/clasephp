<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
         <form action="ejercicio5Destino.php" method="get">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
            </div>
            <br>
            <div>
                <label for="apellidos">
                    Apellidos:
                </label>
                <input type="text" name="apellidos" id="apellidos" placeholder="Introduce tus apellidos">
            </div>
            <br>
            <div>
                <label for="direccion">
                    Direccion:
                </label>
                <input type="text" name="direccion" id="direccion" placeholder="Introduce tu direccion">
            </div>
            <br>
            <div>
                <label for="codigo postal">
                    Codigo postal:
                </label>
                <input type="text" name="cp" id="cp" placeholder="Introduce tu codigo postal">
            </div>
            <br>
            <div>
                <label for="telefono">
                    Telefono:
                </label>
                <input type="text" name="telefono" id="telefono" placeholder="Introduce tu telefono">
            </div>
            <br>
            <div>
                 <label for="email">
                    Email:
                </label>
                <input type="email" id="correo" name="email" placeholder="Introduce tu correo">
            </div>
            <br>
            <div>
                <input type="submit" value="Enviar">
            </div>             
        </form>              
    </body>
</html>
