<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <!--
            Metodo largo
        -->
        Hola <?php echo $_POST["nombre"]; ?><br>
        Tu email es: <?php echo $_POST["email"]; ?>
        
        <br><br>
        <!--
            Metodo recotado
        -->
        Hola <?= $_POST["nombre"] ?><br>
        Tu email es: <?= $_POST["email"] ?>
    </body>
</html>
