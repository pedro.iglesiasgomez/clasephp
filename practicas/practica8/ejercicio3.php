<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio3Destino.php" method="get">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
            </div>
            <br>
            <div>
                 <label for="email">
                    Email:
                </label>
                <input type="email" id="correo" name="email" placeholder="Introduce tu correo">
            </div>
            <br>
            <div>
                <label for="telefono">
                    Telefono:
                </label>
                <input type="text" id="telefono" name="telefono" placeholder="Introduce tu telefono">
            </div>
            <br>
            <div>
                <button>Enviar</button>
            </div>  
            
        </form>    
    </body>
</html>
