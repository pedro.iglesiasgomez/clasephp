<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
            class Vehiculo {
                
                public $matricula;
                private $color;
                protected $encendido;
                public static $ruedas=5; // propiedad de la clase
                
                public function __construct($matricula, $color, $encendido) {
                    $this->matricula = $matricula;
                    $this->color = $color;
                    $this->encendido = $encendido;
                }
                
                //metodo de la clase
                public static function encender() {
                    // no se puede colocar $this dentro de metodos estaticos
                    $this->encendido = true;
                    echo 'Vehiculo encendido <br />';
                }
                
                public function apagar() {
                    $this->encendido = false;
                    echo 'Vehiculo apagado <br />';
                }
                
                static function mensaje() {
                    echo "Este es mi coche";
                }
                
                static function ruedas() {
                    //los metodos estaticos solo pueden acceder a propiedades y metodos estaticos
                    //echo Vehiculo::$ruedas; // forma 1
                    echo self::$ruedas; // forma 2
                    
                }
            }
        ?>
    </body>
</html>
