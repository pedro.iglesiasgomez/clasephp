<?php
    /**
     * Clase Usuario
     */
    class Usuario{
        
        // propiedades
        public $nombre = "defecto";
        private $edad; // propiedad privadad solo accesible por la clase
        protected $telefono; // propiedad protegida se puede acceder desde la clase y desde los hijos
        
        //metodos
        
        // getter
        // metodpos para leer las propiedades
        public function getNombre() {
            return $this->nombre;
        }
        
        public function getEdad() {
            return $this->edad;
        }
        
        public function getTelefono() {
            return $this->telefono;
        }
        
        
        // setter
        // metodos para escribir en las propiedades
        public function setNombre($nombre) {
            $this->nombre = $nombre;
        }
        
        public function setEdad($edad) {
            $this->edad = $edad;
        }
        
        public function setTelefono($telefono) {
            $this->telefono = $telefono;
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            /**
             * vamos a crear objetos de tipo usuario
             */
            $persona =new Usuario(); // Creas un nuevo Usuario(objeto)
            
            //leer el nombre de la persona (defecto
            echo $persona->nombre; // muestra el nombre que se puso por defecto
            
            // escribo en la propiedad 51
            $persona->setEdad(51); // Al usuario le metes la edad puesta
            
            // escribo en la propiedad telefono
            $persona->setTelefono("232323"); // le pones el numero de telefono
            // vuelco en pantalla los valores del objeto
            var_dump($persona);
            
            // escribiendo el nombre de la persona sin utilizar el setter
            $persona->nombre = "Silvia";
           
            
            //para acceder a la propiedad edad tengo que utilizar el setter
            //$persona->setEdad(12);
            //escribiendo la edad de la persona sin utilizar el setter 
            $persona->edad=12; // no puedes acceder por estar la propiedad en privado se podria utilizando el setter o cambiando la propiedad a publica
            
             
            //utilizamos el setter
            //$persona->setTelefono("202020");
            // escribiendo el telefono sin utilizar el setter
            $persona->telefono="202020"; // produce error por ser protected
            var_dump($persona)
            
        ?>
    </body>
</html>
