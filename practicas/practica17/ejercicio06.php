<?php
    include_once './ejercicio06/Vehiculo.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $ford=new Vehiculo("DHH2323", "rojo",false);
            // para llamar a un metodo estatico puedo utilizar
            $ford->mensaje();// forma 1 (no os aconsejo)
            Vehiculo::mensaje(); // forma 2
            $ford::mensaje(); // forma 3 (no os acondejo)
            // muetro todas las propiedades del nuevo objeto
            var_dump($ford); 
            
            // no puedo acceder a una propiedad estatico de esta forma           
            //echo $ford->ruedas();
            // para acceder a una propiedad estatica
            echo Vehiculo::$ruedas; // leyendo la propiedad
            echo $ford::$ruedas; // asi puedo acceder a una propiedad estatica
            
            Vehiculo::$ruedas=6; // escribiendo la propiedad
   
            //Vehiculo::encender(); // esta llamada es correcta pero el metodo esta mal creado
            
            /**
             * concretando
             * para acceder a propiedades estaticas 
             * nombreClase::$nombrePropiedad
             * $nombreObjeto::$nombrePropiedad
             * 
             */
            
            /**
             * Concretando
             * para llamar a metodos estaticos
             * nombreClase::nombreMetodo();
             * $nombreObjeto::nombreMetodo;
             * 
             */
            
            /**
             * Concretando
             * si quiero acceder a mienbros no estaticos
             * 
             * $nombreObjeto->nombrePropiedad();
             * $nombreObjeto->nombreMetodo();
             */
        ?>
    </body>
</html>
