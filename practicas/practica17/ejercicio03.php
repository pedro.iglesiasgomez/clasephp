<?php

    /**
     * Clase usuario
     */
    class Usuario {
        
        
        //propiedades
        public $nombre = "defecto"; // nombre predeterminado
        private $edad; // por defecto el valor es null
        protected $telefono;
        private $apellidos = "defecto";
        private $nombreCompleto;
        
        //metodos
        
        //getters
        public function getNombreCompleto() {
            return $this->nombreCompleto;
        }
        
        public function getApellidos() {
            return $this->apellidos = $apellidos;
            $this->concatenar();
        }
             
        public function getNombre() {
            return $this->nombre;
        }
        
        public function getEdad() {
            return $this->edad;
        }
        
        public function getTelefono() {
            return $this->telefono;
        }
        
        //setters
        
        public function setApellidos($apellidos) {
            $this->apellidos = $apellidos;
            $this->concatenar();
        }
        
        public function setNombre($nombre) {
            $this->nombre = $nombre;
            $this->concatenar();
        }
        
        public function setEdad($edad) {
            $this->edad = $edad;
        }
        
        public function setTelefono($telefono) {
            $this->telefono =$telefono;
        }
        
        // metodo privado
        
        /**
         * asignando a la propiedad nombreCompleto el valor del nombre y los apellidos
         */
        private function concatenar() {
            $this->nombreCompleto = $this->nombre . " " . $this->apellidos; // para que escriba el nombre y los apellidos juntos
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
            /**
             * Crear el objeto
             */
            $persona =new Usuario();
           
            echo $persona->nombre; // leo la propiedad nombre ya que es publica ("defecto")
            
            
            $persona->setEdad(51); // le metes a edad 51 con el setter devido a que la propiedad es privada
            
            $persona->setTelefono("232323"); // asigno un telefono con el setter
            
            $persona->setApellidos("Vazquez Rodrigez"); //asigno los apellidos con el setter
            
            var_dump($persona);
            
            
           
            $persona->nombre="Ramon"; // le asignas un nombre con la propiedad (al utilizar la propiedad no actuliza nombre completo)
            var_dump($persona);
            
            $persona->setNombre("Ramon"); // le añades el nombre con el setter 
            var_dump($persona);
        ?>
    </body>
</html>
