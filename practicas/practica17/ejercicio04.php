<?php
    include "ejercicio04/Coche.php";
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            
        $coche = new Coche(); // instanciamos la clase coche
        $coche->color = 'Rojo'; // Llenamos algunas de las propiedades
        $coche->marca = 'Honda';
        $coche->numero_puertas = 4;
        
        $coche->llenarTanque(10); // echamos gasolina al coche (10 litos)
        
        echo $coche->acelerar(); // aceleramos el coche y mostramos la gasolina que queda
        $coche->acelerar(); // acelero el coche
        echo $coche->acelerar();
        $coche->acelerar();
        
        
        $coche->acelerar();
        
        var_dump($coche);
        ?>
    </body>
</html>
