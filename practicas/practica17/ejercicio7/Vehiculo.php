<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            
            class Vehiculo {
                
                public $matricula;
                private $color;
                protected $encendido;
                
                
                // no se pueden sobrecargar metodos en php
                // no podemos tener dos metodos con el mismo nombre
                
                public function __construct($matricula, $color, $encendido) {
                    $this->matricula = $matricula;
                    $this->color = $color;
                    $this->encendido = $encendido;
                }
                
                public function __construct($matricula, $color) {
                    $this->matricula = $matricula;
                    $this->color = $color;
                    $this->encendido = false;
                }
                
                public function __construct($matricula) {
                    $this->matricula = $matricula,
                    $this->color = "blanco";
                    $this->encendido = false;
                }
                
                public function encender() {
                    $this->encendido = true;
                    echo 'Vehiculo encendido <br />';
                }
                
                public function apagar() {
                    $this->encendido = false;
                    echo 'Vehiculo apagado <br />'
                }
        }
        ?>
    </body>
</html>
