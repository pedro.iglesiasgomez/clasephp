<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            
            class Vehiculo {
                
                public $matricula;
                private $color;
                protected $encendido;
                public $numero;

                /**
                 * Este es una forma de sobrecargar el constructor
                 */
     
                /*
                public function __construct() {
                    $this->numero= func_num_args(); // me indica el numero de argumentos pasados al constructor
                    if($this->numero==1){
                        $this->matricula= func_get_arg(0);
                    }else if ($this->numero==2) {
                        $this->matricula= func_get_arg(0);
                        $this->color= func_get_arg(1);
                    }else {
                        $this->matricula= func_get_arg(0);
                        $this->color= func_get_arg(1);
                        $this->encendido= func_get_arg(2);
                    }
                }
                */
                
                
                /**
                 * 
                 * @param type $matricula
                 * @param type $color
                 * @param type $encendido
                 */
                
                public function __construct($matricula="", $color="", $encendido=false) {
                    $this->matricula = $matricula;
                    $this->color = $color;
                    $this->encendido = false;
                }
              
                
                public function encender() {
                    $this->encendido = true;
                    echo 'Vehiculo encendido <br />';
                }
                
                public function apagar() {
                    $this->encendido = false;
                    echo 'Vehiculo apagado <br />';
                }
            }
        ?>
    </body>
</html>
