<?php
    require './ejercicio05/vehiculo.php';
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
            // instancio un objeto de tipo vehiculo
            // ademas inicializo el vehiculo
            $ford=new Vehiculo("DHH2323", "rojo",false);
            var_dump($ford);
            
            
            $ford->apagar();
            
            //instancio otro objeto de tipo vehiculo
            $renault= new Vehiculo();
            var_dump($renault);
        ?>
    </body>
</html>
