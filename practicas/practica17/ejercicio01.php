<?php
    /**
     * Clase de tipo usuario
     * Clase para probar conocimientos de metodos y propiedades
     */
    class Usuario{
        // definiendo miembros
        
        //propiedades
        public $nombre="defecto"; // propiedad publica con valor predeterminado y tipo string
       // var es igual que poner public no utiliazar
        
        // metodos
        public function setNombre($_nombre="Ramon"){ // metodo publico (setter) nos permite escribir un valor en la propiedad
            $this->nombre=$_nombre;
        }     
        public function getNombre(){ // metodo publico (getter) nos permite leer el valor de la propiedad nombre 
            return $this->nombre;            
        }
    }    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            /*
             * creando un objero de tipo usuario
             * Utilizando directamente la propiedad nombre
             */
        
        
            $persona=new Usuario(); // crea person que es un objeto tipo usuario
            echo $persona->nombre;  // se lee la propiedad nombre de persona ( valor es defecto y es de tipo string)
            $persona->nombre="Silvia"; // se escribe en la propiedad nombre el valor Silvia
            var_dump($persona); // Se comprueba que la variable persona contega el usuario que se ha creado 
            
            /*
             * creando un objero de tipo usuario
             *  utilizar los getter y setter para accerder a lapropiedad nombre
             */
            
            // creo otro usuario
            $persona1=new Usuario();
            echo $persona1->nombre; // leo el valor por defecto de nombre que es "defecto"
            $persona1->setNombre(); // asigno el valor "Ramon" a nombre
            echo $persona1->nombre; //leo el valor de nombre (Ramon)
            
        ?>
    </body>
</html>
