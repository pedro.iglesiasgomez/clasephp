<?php
    /**
     * solicion con if else
     * @param type $numero1
     * @param type $numero2
     */
    function ejercicio7 ($numero1, $numero2){
        if ($numero1==$numero2){
            echo "Los numero son iguales<br>";
        } else {
            echo "Son distintos<br>";
        }
    }
    /*function ejercicio7 ($numero1, $numero2){
        $salida=null;
        if ($numero1==$numero2){
            $salida= "Los numero son iguales<br>";
        } else {
            $salida= "Son distintos<br>";
        }
    }*/
    
    /**
     * esta solucion solo realiza el if
     * @param type $numero1
     * @param type $numero2
     */
    function ejercicio7a ($numero1, $numero2){
        $salida=null;
        if ($numero1==$numero2){
            $salida= "Los numero son iguales<br>";
        }
        echo $salida;
    }
    

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        ejercicio7(5, 5);
        
        ejercicio7(5, 4);
        
        ejercicio7a(5, 4);
        
        ejercicio7a(5, 5);
        
        
        ?>
    </body>
</html>
