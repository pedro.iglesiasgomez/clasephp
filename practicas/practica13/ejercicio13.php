<?php
    function ejercicio13 ($colores){
        foreach ($colores as $color){
            echo "<div style=\"background-color:{$color}; height: 100px\">Caja</div>";
        }   
    }
    function ejercicio13v1 ($colores){
        foreach ($colores as $color){
        ?>
            <div style="background-color:<?= $color ?> ; height: 100px"></div>
        <?php    
        }
    }    

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
        <div>
    <body>
        <?php
            ejercicio13(["red","blue","green"]);
            ejercicio13v1(["yellow", "brown", "orange"]);
        ?>
        
    </body>
</html>
