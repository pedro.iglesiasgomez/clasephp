<?php
    function ejercicio6 ($numeros){
        //realizar el buble para mostrar el array de numeros
        /**
         * Mediante foreach
         * este es mas recomendable para moverte por los arrays
         */
        foreach ($numeros as $valor){
            echo $valor . "<br>";
        }
        
        /**
         * Mediante for
         */
        
        for ($c=0;$c<count($numeros);$c++){
            echo "<br>{$numeros[$c]}<br>";
        }
        
    }
   
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
         ejercicio6([5,4,8,1]);
         
        ?>
    </body>
</html>
