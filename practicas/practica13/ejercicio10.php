<?php

/**
 * Vesion inicial
 * @param type $numeros1
 * @param type $numeros2
 * @param type $numeros3
 * @return type
 */
   /* function ejercicio10 ($numeros1,$numeros2,$numeros3){
        $s1=0;
        $s2=0;
        $s3=0;
        foreach ($numeros1 as $valor){
            $s1+=$valor;
            //$s1=$s1+$valor;
        }
        foreach ($numeros2 as $value) {
            $s2+=$value;
        }
        foreach ($numeros3 as $v) {
            $s3+=$v;
        }
        $suma=$s1 + $s2 + $s3;
        
        
        return $suma;
    }*/
    

/**
 * Version con una funcion de php
 */
   /* function ejercicio10 ($numeros1,$numeros2,$numeros3){
        $n= array_sum($numeros1);
        $n1= array_sum($numeros2);
        $n2= array_sum($numeros3);
        $suma= $n + $n1 +$n2;
        return $suma;
    }*/
  

/**
 * Version con otra funcion que suma el array
 * @param type $numeros1
 * @param type $numeros2
 * @param type $numeros3
 * @return type
 */
    function ejercicio10 ($numeros1,$numeros2,$numeros3){
        $s1= sumArray($numeros1);
        $s2= sumArray($numeros2);
        $s3= sumArray($numeros3);
        return $s1+$s2+$s3;
    }
 
    
 /**
  * Funcion que suma un array
  * @param type $numeros
  * @return type
  */   
    function sumArray ($numeros){
        $salida=0;
        foreach ($numeros as $valor){
            $salida+=$valor;
        }
        return $salida;
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
           $vector1=[1,2,3];
           $vector2=[5,6];
           $vector3=[3,2,3,2];
           echo ejercicio10($vector1, $vector2, $vector3);
           //echo ejercicio10([1,2,3],[5,6],[3,2,3,2])
        ?>
    </body>
</html>
