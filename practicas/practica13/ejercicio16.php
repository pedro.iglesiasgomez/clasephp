<?php
    function ejercicio16($numero1,$numero2){
        global $resultado;
        $resultado= $numero1+$numero2;   
    }
    
    
    function ejercicio16SinGlobal($numero1,$numero2){
    
        return $numero1+$numero2;   
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $resultado=0;
            ejercicio16(10,5);
            echo $resultado;
            $resultado1=0;
            $resultado1=ejercicio16SinGlobal(10, 5);
            echo $resultado1;
        ?>
    </body>
</html>
