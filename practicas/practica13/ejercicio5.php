<?php
    function ejercicio5 ($numero1=1, $numero2=1, $numero3=1){
        $producto= $numero1 * $numero2 * $numero3;
        return $producto;
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo ejercicio5(2, 3, 4) . "<br>";
        
        echo ejercicio5(2, 3) . "<br>";
        
        echo ejercicio5(2) . "<br>";
        
        echo ejercicio5();               
        ?>
        
        <br><?= ejercicio5(5,2,3); ?><br>
    </body>
</html>
