<?php
    function ejercicio17($texto){
       $vocales=0;
       for ($c=0;$c< strlen($texto);$c++){         
           if ($texto[$c]=="a" or $texto[$c]=="e" or $texto[$c]=="i" or $texto[$c]=="o" or $texto[$c]=="u"){              
               $vocales++;               
           }        
       }
       return $vocales;
    }
    
    
    /**
     * Version con bucle y utilizando las tildes
     * @param type $texto
     * @return int
     */
    function ejercicio17v1 ($texto){
        $vocales=0;
        for ($c=0;$c< strlen($texto);$c++){
            //switch (strlen($texto,$c,1)){
            switch (mb_strtolower(mb_substr($texto, $c, 1))){    
                case 'a':
                case 'á':
                case 'e':
                case 'é':
                case 'i':
                case 'í':
                case 'o':
                case 'ó':
                case 'u':
                case 'ú':    
                    $vocales++;
            }           
        }
        return $vocales;
    }   
    
    function ejercicio17v2 ($texto){
        $nvocales=0;
        $vocales=['a','e','i','o','u','á','é','í','ó','ú'];
        for ($c=0; $c<strlen($texto); $c++){
            if (in_array(mb_strtolower(mb_substr($texto, $c, 1)), $vocales)){
                $nvocales++;
            }
        }
        return $nvocales;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            echo ejercicio17("ejemplo de clase");
            echo ejercicio17v1("Ejémplo de clase"); 
            echo ejercicio17v2("Ejemplo de clase");
        ?>
    </body>
</html>
