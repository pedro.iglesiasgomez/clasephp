<?php

    /**
     * version en la que recorro el array con un foreach
     * @param type $numeros
     * @return type
     */
    function ejercicio14 ($numeros){
        $suma=0;
        foreach ($numeros as $valor){
            $suma+=$valor;
        }
        
        return $suma;
    }
    
    /**
     * Utilizando la funcion de php array_sum
     */
    function ejercicio14v1 ($numeros){
        return array_sum($numeros);
    }
    
    
    function ejercicio14v2 (...$numeros){
        return array_sum($numeros);
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            echo ejercicio14([2,4,6,8,10]);
            echo ejercicio14v1([2,4,6,8,10]);
            echo ejercicio14v2(2,4,6,8,10);
        ?>
    </body>
</html>
