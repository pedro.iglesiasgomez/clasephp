<?php
    function imprimeDosNumeros ($dato1, $dato2){
        $suma= $dato1 + $dato2;
        echo $suma . "<br>";
    }
    
    function imprimeTresNumeros ($numero1, $numero2, $numero3){
        $suma1 = $numero1 + $numero2 + $numero3;
        echo "{$suma1}<br>";
    }
    
    /**
     * 
     * podemos utilizar argumentos opcionales para solo hacer una funcio
     */
    
    function ejercicio3c ($numero1_1=0, $numero2_2=0, $numero3_3=0){
        $suma2 = $numero1_1 + $numero2_2 + $numero3_3;
        echo $suma2 . "<br>";
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            imprimeDosNumeros(5, 5);
            imprimeTresNumeros(10, 10, 12);
            ejercicio3c(5, 10);
        ?>
    </body>
</html>
