<?php

    /**
     * Version utilizando la version de php strrev
     * @param type $texto
     */
    function ejercicio12 ($texto){
        echo strrev($texto) . "<br>"; // me coloca el texto al reves       
    }
    
    /**
     * Version en la que convierto el string en un array con la version
     * str_split
     * @param type $texto
     */
    function ejercicio12v1 ($texto){
        $vector= str_split($texto); // me crea un array con el string
        for($c= count($vector)-1; $c>=0; $c--){
            echo $vector[$c];
        }
    }
    /**
     * Version que muestro el texto girado tratandolo como un array
     * @param type $texto
     */
    function ejercicio12v2 ($texto){
        for($c= strlen($texto)-1; $c>=0; $c--){
            echo $texto[$c]; // voy a tratar el texto como si fuera un array
        }
    }
    
    /**
     * Funcion que realiza lo mismo que la funcion de php strrev
     * @param type $texto
     * @return string
     */
    function mistrrev($texto){
        $resultado="";
        for($c= strlen($texto)-1; $c>=0; $c--){
        $resultado=$resultado . $texto[$c]; // voyu a tratar el texto como si fuera un array
        }
        return $resultado;
    }
    
    /**
     * Version en el que utilizo mi propia funcion strrev
     * @param type $texto
     */
    function ejercicio12v3 ($texto){
        $resultado= mistrrev($texto);
        echo "<br>{$resultado}<br>";
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            ejercicio12("Estoy haciendo el ejercicio 12 de la practica 13");
            ejercicio12v1("Estoy haciendo el ejercicio 12 de la practica 13");
            ejercicio12v2("Estoy haciendo el ejercicio 12 de la practica 13");
            ejercicio12v3("Estoy haciendo el ejercicio 12 de la practica 13");
        ?>
    </body>
</html>
